#!bin/sh

doit()
{
    local a=$1

    echo Building assembler $m $a
    cc -target $a-unknown-freebsd14.0 -o h.$a h.$a.s -nostdlib -Wl,-e -Wl,qemu_start -static
}

doit armv7
doit amd64
doit aarch64
doit i386
doit mips
doit mipsel
doit mips64
doit powerpc
doit powerpc64
# doit powerpc powerpc64le
doit riscv64
